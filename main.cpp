#include <drogon/drogon.h>
#include "structs.h"

using namespace drogon;

bool startsWith(const std::string_view text, std::string_view start)
{
    if (start.size() > text.size()) {
        return false;
    }

    return text.substr(0, start.size()) == start;
}

std::optional<std::string_view> removePrefix(std::string_view text, std::string_view start)
{
    if (startsWith(text, start)) {
        return text.substr(start.size());
    }
    return std::nullopt;
}


auto getForm = [](const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback)
{
    std::vector<Person> persons {
        Person {
            .id = 1,
            .name = "Jonah",
            .jid = "jbb@kaidan.im",
            .balance = 1204,
        },
        Person {
            .id = 2,
            .name = "Robert",
            .jid = "zatrox@kaidan.im",
            .balance = 12001,
        }
    };
    
    HttpViewData data;
    data.insert("title", "Moin");
    data.insert("users", persons);
    auto resp = HttpResponse::newHttpViewResponse("moin.csp", data);
    callback(resp);
};

auto postForm = [](const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback) 
{
    std::vector<int> uids;

    for (const auto &[key, value] : req->parameters()) {
        if (auto uid = removePrefix(key, "_uid")) {
            uids.push_back(std::atoi(uid->data()));
        }
    }

    HttpViewData data;
    data.insert("title", "Moin");
    data.insert("uids", uids);
    auto resp = HttpResponse::newHttpViewResponse("added.csp", data);
    callback(resp);
};

int main()
{
    // Set HTTP listener address and port
    drogon::app().addListener("127.0.0.1", 8080);
    // Load config file
    // drogon::app().loadConfigFile("../config.json");
    // Run HTTP framework,the method will block in the internal event loop

    app().registerHandler("/", getForm, { Get })
         .registerHandler("/add", postForm, { Post })
         .setLogLevel(trantor::Logger::LogLevel::kTrace);

    drogon::app().run();
    return 0;
}
