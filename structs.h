#include <string>

struct Person
{
    int64_t id;
    std::string name;
    std::string jid;
    int32_t balance;
};

struct Expense
{
    int64_t id;
    int32_t price;
    int64_t timestamp;
    std::string description;
    int64_t paidBy;
//    PersonIds persons;
};
